/**
 * Created by iyegoroff on 17/01/15.
 */

///<reference path='rx/rx.all.d.ts' />

declare module rxia {
    export interface Event {
        distance: number;
        angle: number;
        centerX: number;
        centerY: number;
        target: EventTarget;
        scale: number;
        rotation: number;
    }

    export interface Options {
        preventDefault?: boolean;
    }

    export interface TapOptions extends Options {
        time?: number;
        threshold?: number;
    }

    export interface DoubleTapOptions extends TapOptions {
        interval?: number;
        posThreshold?: number;
    }

    export interface MultiTapOptions extends DoubleTapOptions {
        taps?: number;
    }

    export interface SwipeOptions extends Options {
        threshold?: number;
        velocity?: number;
    }

    export interface PressOptions extends Options {
        time?: number;
        threshold?: number;
    }

    export interface GestureOptions extends Options {
        threshold?: number;
    }

    export interface Gesture {
        start: Rx.Observable<Event>;
        move: Rx.Observable<Event>;
        end: Rx.Observable<Event>;
    }

    export function xOf(event: Event): number;
    export function yOf(event: Event): number;

    export function tap(element: Element, options?: TapOptions): Rx.Observable<Event>;

    export function doubleTap(element: Element, options?: DoubleTapOptions): Rx.Observable<Event>;

    export function multiTap(element: Element, options?: MultiTapOptions): Rx.Observable<Event>;

    export function swipe(element: Element, options?: SwipeOptions): Rx.Observable<Event>;

    export function press(element: Element, options?: PressOptions): Rx.Observable<Event>;

    export function pan(element: Element, options?: GestureOptions): Gesture;

    export function pinch(element: Element, options?: GestureOptions): Gesture;

    export function rotate(element: Element, options?: GestureOptions): Gesture;
}
